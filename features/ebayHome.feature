Feature: Les fonctionnalités de Ebay homepage

  @sc1
  Scenario: scenario qui teste la recherche avancée
    Given Je suis sur la page daccueil
    When Lorsque je clique sur le lien recherce avancée
    Then je navigue vers la page de recherche avancée

  @Sc3
  Scenario: scenario pour faire une recherche sur la home page
    Given Je suis sur la page daccueil
    When Lorsque je recherche 'Laptop'
    Then je valide quau moins 1000 resultat sont affichés

  @Sc4
  Scenario: scenario pour faire une recherche sur la home page
    Given Je suis sur la page daccueil
    When Lorsque je recherche 'Selenium'
    Then je valide quau moins 800 resultat sont affichés

  @Sc5
  Scenario: scenario pour faire une recherche sur la home page
    Given Je suis sur la page daccueil
    When Lorsque je recherche 'iphone'
    Then je valide quau moins 8000 resultat sont affichés
    
  @Sc6
  Scenario: scenario pour faire une recherche dans la section categorie
    Given Je suis sur la page daccueil
    When Lorsque je recherche 'selenium' dans la categorie 'Books'
    Then je valide quau moins 80 resultat sont affichés
    
  @Sc7
  Scenario: scenario pour faire une recherche dans la section categorie
    Given Je suis sur la page daccueil
    When Lorsque je recherche 'iphone' dans la categorie 'Cell Phones & Accessories'
    Then je valide quau moins 80 resultat sont affichés
