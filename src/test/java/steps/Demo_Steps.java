package steps;

import io.cucumber.java.en.*;


public class Demo_Steps {
	
	@Given("I want to open OrangeHRM in Chrome")
	public void i_want_to_open_OrangeHRM_in_Chrome() {
	    System.out.println("Ma premiere etape");
	}

	@Given("I wand to maximise Browser")
	public void i_wand_to_maximise_Browser() {
		System.out.println("Ma deuxieme etape");
	}

	@When("I write a valid username")
	public void i_write_a_valid_username() {
		System.out.println("Ma troisieme etape");
	}

	@When("I write a valid password")
	public void i_write_a_valid_password() {
		System.out.println("Ma quatrieme etape");
	}

	@When("I clic submit button")
	public void i_clic_submit_button() {
		System.out.println("Ma cinquieme etape");
	}

	@Then("The home page is loading")
	public void the_home_page_is_loading() {
		System.out.println("Ma sixieme etape");
	}

	@Then("The Dashboard is visible")
	public void the_Dashboard_is_visible() {
		System.out.println("Ma septieme etape");
	}


}
