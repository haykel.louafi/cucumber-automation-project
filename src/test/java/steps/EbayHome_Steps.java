package steps;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;


public class EbayHome_Steps {
	WebDriver driver;
	
	@Given("Je suis sur la page daccueil")
	public void je_suis_sur_la_page_daccueil() {
	    WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.ebay.ca/");
	}

	@When("Lorsque je clique sur le lien recherce avancée")
	public void lorsque_je_clique_sur_le_lien_recherce_avancée() {
		driver.findElement(By.id("gh-as-a")).click();
	}

	@Then("je navigue vers la page de recherche avancée")
	public void je_navigue_vers_la_page_de_recherche_avancée() throws Exception {
	    String urlattendu = "https://www.ebay.ca/sch/ebayadvsearch";
	    String Titreattendu = "eBay Search: Advanced Search";
	    String urlobtenu = driver.getCurrentUrl();
	    String titreObtenu = driver.getTitle();
	    if(! titreObtenu.equalsIgnoreCase(Titreattendu)) {
	    	fail("Le titre de la page n'est pas celui attendu");
	    }
	    
	    Assert.assertEquals("Valider qu'on est dirigé vers la page recherche avancée", urlattendu, urlobtenu);
	    Thread.sleep(4000);
	    driver.quit();
	}
	
	//Scenario: scenario pour faire une recherche sur la home page
	@When("Lorsque je recherche {string}")
	public void lorsque_je_recherche_un_Laptop(String item) {
		driver.findElement(By.id("gh-ac")).sendKeys(item);
		driver.findElement(By.id("gh-btn")).click();
	}

	@Then("je valide quau moins {int} resultat sont affichés")
	public void je_valide_quau_moins_resultat_sont_affichés(Integer counter) {
		String Nbelement = driver.findElement(By.cssSelector("h1.srp-controls__count-heading>span.BOLD:first-child")).getText();
		String Nbelement2 = Nbelement.replace(",", "");
		int compteur=Integer.parseInt(Nbelement2);
		if(compteur<counter) {
			fail("Le resultat est inferieur a "+counter);
		}
		driver.quit();
	}
	
	@When("Lorsque je recherche {string} dans la categorie {string}")
	public void lorsque_je_recherche_dans_la_categorie(String item, String categorie) throws Exception {
		driver.findElement(By.id("gh-ac")).sendKeys(item);
		driver.findElement(By.id("gh-cat")).click();
		Thread.sleep(2);
		List<WebElement> listCategorie = driver.findElements(By.cssSelector("select#gh-cat>option"));
		for(WebElement el:listCategorie) {
			if(el.getText().equalsIgnoreCase(categorie)) {
				el.click();
				break;
			}
		}
		Thread.sleep(2000);
		driver.findElement(By.id("gh-btn")).click();
	}
}
