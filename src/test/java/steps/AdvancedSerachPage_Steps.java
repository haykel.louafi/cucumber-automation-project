package steps;

import static org.junit.Assert.fail;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class AdvancedSerachPage_Steps {
	WebDriver driver;
	
	@Given("Je suis sur la page recherche avancée")
	public void je_suis_sur_la_page_recherche_avancée() {
	    WebDriverManager.chromedriver().setup();
	    driver = new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("https://www.ebay.ca/sch/ebayadvsearch");
	}

	@When("Lorsque je clique sur le logo ebay")
	public void lorsque_je_clique_sur_le_logo_ebay() {
	    driver.findElement(By.id("gh-la")).click();
	}

	@Then("je navigue vers la home page")
	public void je_navigue_vers_la_home_page() {
	    String TitreObt = driver.getTitle();
	    String UrlObt = driver.getCurrentUrl();
	    String TitreAtt = "Electronics, Cars, Fashion, Collectibles & More | eBay";
	    String UrlAtt = "https://www.ebay.ca/";
	    if(! TitreObt.equalsIgnoreCase(TitreAtt)) {
	    	fail("La page est differente de la page attendu");
	    }
	    Assert.assertEquals(UrlAtt, UrlObt);
	    driver.quit();
	}

}
