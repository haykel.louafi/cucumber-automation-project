package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"features"}, 			//chemin du dossier contenant les features
		glue = {"steps"},          			//chemin du dossier contenant les steps definitions
		dryRun = false, 					// true: cherche les features qui n'ont pas de glue code et les runner
		monochrome = true,					//formatter l'affichage dans la console 
		//tags = "@Sc7",		//selectionner les scenario a executer, pour excuter +sieurs tag "@sc1 or @sc2"
		//name = "of",						//cherrche et execute les scenarios qui contiennent l'expression
		plugin = {"pretty","json:target/json-report/cucumber.json"}  //creer un rapport nommé report1 en format html
		
		)
public class TestRunner {
	
}
